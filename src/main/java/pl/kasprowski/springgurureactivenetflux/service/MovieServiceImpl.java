package pl.kasprowski.springgurureactivenetflux.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.kasprowski.springgurureactivenetflux.domain.Movie;
import pl.kasprowski.springgurureactivenetflux.domain.MovieEvent;
import pl.kasprowski.springgurureactivenetflux.repository.MovieRepository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.time.Duration;
import java.util.Date;

@Service
public class MovieServiceImpl implements MovieService {

    private final MovieRepository repository;

    @Autowired
    public MovieServiceImpl(final MovieRepository repository) {
        this.repository = repository;
    }

    @Override
    public Flux<MovieEvent> events(String movieId) {
        return Flux.<MovieEvent>generate(sink -> sink.next(new MovieEvent(movieId, new Date())))
                .delayElements(Duration.ofSeconds(1));
    }

    @Override
    public Mono<Movie> getById(String id) {
        return repository.findById(id);
    }

    @Override
    public Flux<Movie> getAll() {
        return repository.findAll();
    }
}
