package pl.kasprowski.springgurureactivenetflux.service;

import pl.kasprowski.springgurureactivenetflux.domain.Movie;
import pl.kasprowski.springgurureactivenetflux.domain.MovieEvent;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public interface MovieService {
    Flux<MovieEvent> events(String movieId);

    Mono<Movie> getById(String id);

    Flux<Movie> getAll();
}
