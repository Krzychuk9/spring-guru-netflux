package pl.kasprowski.springgurureactivenetflux.bootstrap;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;
import pl.kasprowski.springgurureactivenetflux.domain.Movie;
import pl.kasprowski.springgurureactivenetflux.repository.MovieRepository;
import reactor.core.publisher.Flux;

import java.util.UUID;

@Slf4j
@Component
public class BootstrapCLR implements CommandLineRunner {

    private final MovieRepository movieRepository;

    @Autowired
    public BootstrapCLR(final MovieRepository movieRepository) {
        this.movieRepository = movieRepository;
    }

    @Override
    public void run(String... args) {
        movieRepository.deleteAll()
                .thenMany(Flux.just("Silence of the Lambdas", "AEon Flux", "Enter the Mono<Void>", "The Fluxxinator",
                        "Back to the Future", "Meet the Fluxes", "Lord of the Fluxes")
                        .map(title -> new Movie(UUID.randomUUID().toString(), title))
                        .flatMap(movieRepository::save))
                .subscribe(null, null, () -> movieRepository.findAll().subscribe(movie -> log.info(movie.toString())));
    }
}
