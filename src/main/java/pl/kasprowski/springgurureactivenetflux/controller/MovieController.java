package pl.kasprowski.springgurureactivenetflux.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import pl.kasprowski.springgurureactivenetflux.domain.Movie;
import pl.kasprowski.springgurureactivenetflux.domain.MovieEvent;
import pl.kasprowski.springgurureactivenetflux.service.MovieService;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@RestController
@RequestMapping("/movies")
public class MovieController {

    private final MovieService movieService;

    @Autowired
    public MovieController(final MovieService movieService) {
        this.movieService = movieService;
    }

    @GetMapping(value = "/{id}/events", produces = MediaType.TEXT_EVENT_STREAM_VALUE)
    public Flux<MovieEvent> events(@PathVariable String id) {
        return movieService.events(id);
    }

    @GetMapping("/{id}")
    public Mono<ResponseEntity<Movie>> getById(@PathVariable String id) {
        return movieService.getById(id)
                .map(ResponseEntity::ok)
                .defaultIfEmpty(ResponseEntity.notFound().build());
    }

    @GetMapping
    public Flux<Movie> getAll() {
        return movieService.getAll();
    }
}
