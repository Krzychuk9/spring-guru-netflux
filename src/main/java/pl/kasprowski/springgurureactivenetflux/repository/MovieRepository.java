package pl.kasprowski.springgurureactivenetflux.repository;

import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import org.springframework.stereotype.Repository;
import pl.kasprowski.springgurureactivenetflux.domain.Movie;

@Repository
public interface MovieRepository extends ReactiveMongoRepository<Movie, String> {
}
