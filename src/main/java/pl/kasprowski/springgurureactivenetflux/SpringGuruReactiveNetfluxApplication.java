package pl.kasprowski.springgurureactivenetflux;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringGuruReactiveNetfluxApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringGuruReactiveNetfluxApplication.class, args);
    }
}
